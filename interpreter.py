"""Project - Combine correspondence
Alan Giovanni Lomelí Bautista
16310215        8M
<a16310215@ceti.mx>

In a plain text file, register the following person information :
(name, last_name, last_mother_name, job, company, street, exterior_number,
 interior_number, colony, municipality, state, postal_code, phone_number,
 email_address and birthday)
It could be any format and any number of persons. The format is taken
from the 'persons.txt' file.

In another plain text file, make an 'office', and with the information
presented in 'office-template.txt' file, replace the placeholder
elements with the person information for each individual.

"""


import re                       # For regular expressions.
from datetime import datetime   # For getting today's date.
from typing import (Dict,
                    List,
                    Pattern,
                    Optional,
                    Match,
                    Any,
                    AnyStr)     # For static type checking.
from fpdf import FPDF           # PDF Generator support

# Global variables


class Interpreter:
    """Interpreter class.
    It holds 2 private properties:
    - _db_filename:              File in where it grabs the information.
    - _office_template_filename: File in where prints the generated persons.
    """

    # Constructor
    def __init__(self,
                 db_filename: str = "persons.txt",
                 office_template_filename: str = "office-template.txt"):
        self._db_filename: str = db_filename
        self._office_template_filename: str = office_template_filename
        self._get_attribute: Pattern[AnyStr] = \
            re.compile(r'(^[\s]*|[\w]*)\s*=\s*(.*)')

    # Public method(s) ########################################################
    
    def load_persons(self) -> List[Dict[str, str]]:
        """Get a dictionary of persons based on the given file."""
        persons: List[Dict[str, str]] = []
        person: Dict[str, Any] = {}
        db: List[Any] = []
        with open(self._db_filename, 'r') as db_file:
            db = db_file.read().split('\n\n')
        # Filter empty lines
        db = list(filter(lambda data: len(data) > 0, db))
        for person_file in db:
            for line in person_file.split('\n'):
                attribute: Optional[Match[AnyStr]] = \
                    self._get_attribute.match(line)
                if attribute:
                    person[attribute.group(1)] = attribute.group(2)
            person['age'] = self._get_age(person['birthday'])
            persons.append(person.copy())
            person.clear()
        return persons

    def load_person(self, person_id: int) -> Dict[str, str]:
        """Load a specific person based on the given person string."""
        person: Dict[str, str] = {}
        persons = filter(lambda person:
                         person['id'] == str(person_id),
                         self.load_persons())
        for person in persons:
            person = person
        return person

    def add_person(self, person: Dict[str, Any]) -> bool:
        """Add the given person to the database defined in '_db_filename'."""
        # Get the last and mayor id:
        current_id: int = 0
        persons: List[Dict[str, Any]] = self.load_persons()
        if not not persons:
            current_id = int(max(list(map(lambda p: p['id'], persons))))
        # Append it to the current person
        person['id'] = current_id + 1
        # Append the new person to file:
        with open(self._db_filename, 'a') as db_file:
            db_file.writelines('\n')
            for attribute in person:
                if attribute != 'age':
                    db_file.writelines(f'{attribute} = {person[attribute]}\n')
        return True

    def modify_person(self, person_id: int, person: Dict[str, str]) -> bool:
        """Modify a person from the given person."""
        persons: List[Dict[str, Any]] = []
        operation_successful: bool = True
        # Get all the persons except the person to search:
        persons = self._filter_person(person_id)
        # Append the new user:
        person['id'] = str(person_id)
        persons.insert(person_id - 1, person)
        # Overwrite the file with the new information:
        self._overwrite_person_file(persons)
        return operation_successful

    def delete_person(self, person_id: int) -> bool:
        """Delete a person from the file given the person_id."""
        persons: List[Dict[str, Any]] = []
        operation_successful: bool = True
        # Get all the persons except the person to search:
        persons = self._filter_person(person_id)
        # Overwrite the file with the new information:
        self._overwrite_person_file(persons)
        return operation_successful

    # Office specific functions ###############################################

    def create_office(self, person: Dict[str, str]) -> str:
        """Create an office based on the given person and the given office
        template."""
        new_office: str = self._load_office_template()
        for attribute in person:
            new_office = new_office.replace('[' + attribute + ']',
                                            person[attribute])
        return new_office

    def generate_pdf(self, message: str) -> FPDF:
        """
        Generate a pdf based on the given message.
        """
        pdf = FPDF(format='letter')
        pdf.add_page()
        pdf.set_font('Arial', size=12)
        for line in message.split('\n'):
            # Highlight the headers:
            if line == 'Personal information:' or \
               line == 'Location information:' or \
               line == 'Contact information:':
                pdf.set_font('Arial', 'B', size=12)
                pdf.cell(200, 5, txt=line, ln=1, align='J')
            else:
                pdf.set_font('Arial', size=12)
                pdf.cell(200, 5, txt=line, ln=1, align='J')
        return pdf

    # Private method(s) #######################################################
    
    def _overwrite_person_file(self, persons: List[Dict[str, Any]]) -> None:
        """Write the given data on the database file."""
        with open(self._db_filename, 'w') as db_file:
            for person in persons:
                for attribute in person:
                    if attribute != 'age':
                        db_file.writelines(
                            f'{attribute} = {person[attribute]}\n')
                # Avoid double newline at the end of the file:
                if person['id'] != persons[-1]['id']:
                    db_file.writelines('\n')

    def _filter_person(self, person_id: int) -> List[Dict[str, Any]]:
        """Filter the given person based on """
        return list(filter(lambda person: str(person_id) != person['id'],
                           self.load_persons()))

    def _get_age(self, birthday: str) -> str:
        """Get the person's age based on their birthday."""
        aux_birthday: List[int] = list(map(lambda x: int(x),
                                           birthday.split('/')))
        present_day: List[int] = list(map(lambda x: int(x),
                                          datetime
                                          .today()
                                          .strftime('%Y/%m/%d')
                                          .split('/')))
        age: int = present_day[0] - aux_birthday[0]
        # Check months and days for reduce
        months_difference: int = present_day[1] - aux_birthday[1]
        days_difference: int = present_day[2] - aux_birthday[2]
        if months_difference <= 0:
            if days_difference <= 0:
                age -= 1
        return str(age)

    def _load_office_template(self) -> str:
        """Load the office template into memory."""
        office_template: str = ""
        with open(self._office_template_filename) as office_template_file:
            office_template = office_template_file.read()
        return office_template
