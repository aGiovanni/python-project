import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonDrawerComponent } from './person-drawer.component';

describe('PersonDrawerComponent', () => {
  let component: PersonDrawerComponent;
  let fixture: ComponentFixture<PersonDrawerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonDrawerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
