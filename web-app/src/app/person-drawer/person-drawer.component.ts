import { Component, OnInit } from '@angular/core';

import { DataService } from '../services/data.service';

@Component({
  selector: 'app-person-drawer',
  templateUrl: './person-drawer.component.html',
  styleUrls: ['./person-drawer.component.scss']
})
export class PersonDrawerComponent implements OnInit {

  constructor( public data: DataService ) { }

  ngOnInit(): void {
  }

}
