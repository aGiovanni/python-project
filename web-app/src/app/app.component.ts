import { Component } from '@angular/core';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor( public data: DataService ) {}

  public sidenavButton(): string {
    return this.data.sidenavOpen ? 'chevron_left' : 'chevron_right';
  }

}
