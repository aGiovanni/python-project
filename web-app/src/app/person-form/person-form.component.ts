import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AskConfirmationComponent } from '../dialogs/ask-confirmation/ask-confirmation.component';
import { PersonEmailComponent } from '../dialogs/person-email/person-email.component';

import { DataService } from '../services/data.service';
import { Person } from '../shared/person-format';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.scss'],
})
export class PersonFormComponent implements OnInit {

  @Input() person: Person = null;

  constructor( public data: DataService,
               public dialog: MatDialog,
               public emailDialog: MatDialog ) { }

  ngOnInit(): void {
  }

  // Person specific methods //////////////////////////////////////////////////

  onSubmit() {
    if ( this.data.isNewPerson ) {
      this.data.addPerson( this.person );
    } else {
      this.data.modifyPerson( this.person );
    }
    this.data.fetchPersons();
  }

  public onDeletePerson(): void {
    const dialogRef = this.dialog.open(AskConfirmationComponent, {
      width: '320px',
      data: { message: 'Are you sure you want to delete this person?' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result ) this.deletePerson();
    });
  }

  private deletePerson(): void {
    if (this.data.personState == this.person) this.data.clearPerson();
    this.data.deletePerson( this.person );
    // Reload the changes by fetching to the server
    this.data.fetchPersons();
  }

  // Office specific method(s) ////////////////////////////////////////////////

  /**
   * @function sendOffice.
   */
  public sendOffice( sendToEmail: boolean,
                     officeType: string  ): void {
    this.data.sendOffice(
      parseInt(this.person.id),
      {
        sent_to_email: sendToEmail,
        office_type: officeType,
        email_recipient: ""
      });
  }

  public sendOfficeToEmail(): void {
    const dialogRef = this.dialog.open(PersonEmailComponent, {
      width: '320px',
      data: { email: this.person.email_address }
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result.confirm ) {
        this.data.sendOffice(
          parseInt(this.person.id),
          {
            sent_to_email: true,
            office_type: result.officeType,
            email_recipient: result.data
          });
      }
    });
  }

}
