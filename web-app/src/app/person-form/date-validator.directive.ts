import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

import { dateValidator } from '../shared/date-validator';

@Directive({
  selector: '[appDateValidator]',
  providers: [{ provide: NG_VALIDATORS,
                useExisting: DateValidatorDirective,
                multi: true }]
})
export class DateValidatorDirective implements Validator {

  @Input('appDateValidator') date: string;

  validate( control: AbstractControl ): { [key: string]: any } | null {
    return this.date
      ? dateValidator(new RegExp(this.date, 'i'))(control)
      : null;
  }

}
