/**
 * ApiService class.
 *
 * It handles all the HTTP petitions sent to the server, as well as
 * error handling of those petitions.
 */

import { Injectable } from '@angular/core';
import { HttpClient,
         HttpHeaders,
         HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Person } from '../shared/person-format';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  /* Default URL */
  private HTTP_URL: string = 'http://localhost:5000';

  constructor( private http: HttpClient ) { }

  // HTTP Calls ///////////////////////////////////////////////////////////////

  /**
   * @function getPersons.
   *
   * It receives all the available persons that the server would have,
   * the format in which the objects are received is defined in
   * 'app/shared/person-format'.
   *
   * @returns an *Observable* which contains the array of persons
   * obtained.
   */
  getPersons(): Observable<Array<Person>> {
    return this.http.get<Array<Person>>(
      `${this.HTTP_URL}/users`
    ).pipe(catchError(this.handleError));
  }

  /**
   * @function createPerson.
   *
   * It sends the form information to the server to append the new
   * person to the database (in this case, a text file).
   *
   * @param person - <Person> The *Person* object containing the new
   *                 information.
   *
   * @returns an *Observable* with the response of the server.
   */
  createPerson( person: Person ): Observable<any> {
    // TODO: function in backend
    return this.http.post<any>(
      `${this.HTTP_URL}/users`,
      person,
      httpOptions
    ).pipe(catchError(this.handleError));
  }

  /**
   * @function modifyPerson.
   *
   * Modify a specific person by the ID and replaces their information
   * with given new person.
   *
   * @param id - <number> The person's identification.
   * @param person - <Person> The Person object to replace.
   *
   * @returns an *Observable* with the response of the server.
   */
  modifyPerson( id: number, person: Person ): Observable<any> {
    return this.http.post<any>(
      `${this.HTTP_URL}/users/${id}`,
      person,
      httpOptions
    ).pipe(catchError(this.handleError));
  }

  /**
   * @function deletePerson.
   *
   * Delete a specific person through it's ID.
   *
   * @param idPerson - <number> Person's identification.
   *
   * @returns an *Observable* with the response of the server.
   */
  deletePerson( idPerson: number ): Observable<any> {
    return this.http.delete<any>(
      `${this.HTTP_URL}/users/${idPerson}`
    ).pipe(catchError(this.handleError));
  }

  /**
   * @function sendOffice.
   *
   * Sends
   */
  sendOffice( idPerson: number, options: any ): Observable<any> {
    return this.http.post<any>(
      `${this.HTTP_URL}/offices/${idPerson}`,
      options,
      httpOptions
    ).pipe(catchError(this.handleError));
  }

  // Error handling ///////////////////////////////////////////////////////////
  
  private handleError(error: HttpErrorResponse) {
    let message: Observable<never>;
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      message = throwError('An error ocurred: ' + error.error.message);
      // console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      message = throwError(`${error.status}`);
      // For debugging purposes:
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return message;
  }
}
