/**
 * DataService class.
 *
 * It provides the components with instances of all the persons obtained by the API.
 */

import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ApiService } from './api.service';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Person } from '../shared/person-format';
import { ConfirmComponent } from '../dialogs/confirm/confirm.component';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // Component functionality properties ///////////////////////////////////////
  
  public sidenavOpen: boolean = false;
  public isNewPerson: boolean = false;

  // Data handling properties /////////////////////////////////////////////////
  
  private _person: BehaviorSubject<Person> = new BehaviorSubject<Person>(null);
  private _persons: BehaviorSubject<Array<Person>> =
    new BehaviorSubject<Array<Person>>([]);

  // Getter(s) ////////////////////////////////////////////////////////////////
  
  get person(): Observable<Person> { return this._person.asObservable(); }

  get personState(): Person | null { return this._person.getValue(); }
  
  get persons(): Observable<Array<Person>> {
    return this._persons.asObservable();
  }

  // Dependency injector (and constructor) ////////////////////////////////////
  
  constructor(
    private api: ApiService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.fetchPersons();
  }

  // Component specific method(s) /////////////////////////////////////////////

  public toggleSidenav(): void { this.sidenavOpen = !this.sidenavOpen; }  

  public newPerson(): void {
    this.isNewPerson = true;
    this._person.next({});
  }

  public selectPerson( person_id: string ): void {
    this.isNewPerson = false;
    this._person.next(
      this._persons.getValue().filter(person => person.id == person_id)[0]
    );
  }

  public clearPerson(): void {
    this._person.next(null);
  }

  // API handling method(s) ///////////////////////////////////////////////////

  /**
   * @function @fetchPersons.
   */
  public fetchPersons(): void {
    this._snackBar.open('Fetching persons...');
    this.api.getPersons().subscribe(
      ( response: Array<Person> ) => {
        this._persons.next( response );
        console.log(this._persons.getValue());
      },
      error => {
        this._snackBar.open(
          'An error ocurred, please check the console to see the details.',
          'Ok'
        )
        console.log( error );
        this._persons.next( [] );
      },
      () => { this._snackBar.open('Fetching successful!', 'Ok'); }
    );
  }

  /**
   * @function addPerson.
   */
  public addPerson( person: Person ): void {
    this._snackBar.open('Adding person...');
    this.api.createPerson( person ).subscribe(
      response => {},
      error => {
        console.log(error);
      },
      () => {
        this._snackBar.open('User added to the database!', 'Ok');
      }
    );
  }

  /**
   * @function modifyPerson.
   *
   * It calls the *api* @service to send the request and notify with a
   * *snackBar* the server's response.
   */
  public modifyPerson( person: Person ): void {
    this._snackBar.open('Modifying person...');
    this.api.modifyPerson( parseInt(person.id), person ).subscribe(
      response => {},
      error => {
        console.log(error);
      },
      () => {
        this._snackBar.open('User modified and saved to the database!', 'Ok');
      }
    );
  }

  /**
   * @function deletePerson.
   */
  public deletePerson( person: Person ): void {
    this._snackBar.open('Deleting person...', '');
    this.api.deletePerson(parseInt(person.id)).subscribe(
      response => {},
      error => {
        console.log(error);
        this._snackBar.open('An error ocurred. Check console for details',
                            'Ok');
      },
      () => {
        this._snackBar.open('User deleted in the database!', 'Ok');
      }
    );
  }

  /**
   * @function sendOffice.
   *
   * Sends the person selected to write the office. It takes a couple
   * of arguments in *options* parameter:
   *
   * @param sent_to_email   -> <bool> whether the send to an email or
   *                           send locally.
   * @param office_type     -> <string> Office format. could be "txt"
   *                           or "pdf".
   * @param email_recipient -> [optional] <string> The email to send 
   *                           the offices.
   */
  public sendOffice( idPerson: number, options: any ): void {
    this._snackBar.open('Generating office file...', '');
    this.api.sendOffice( idPerson, options ).subscribe(
      response => {},
      error => {
        console.log(error);
        this._snackBar.open('An error ocurred. Check console for details',
                            'Ok');
      },
      () => {
        const msgConfirm = options.sent_to_email
          ? `Check ${options.email_recipient} inbox to see the file!`
          : 'Check the project root to see the generated file.';
        const dialogRef = this.dialog.open(ConfirmComponent, {
          width: '320px',
          data: {
            title: `"office-${idPerson}.${options.office_type}"` +
                    " successfully created!",
            message: msgConfirm
          }
        });
      }
    );
  }
}
