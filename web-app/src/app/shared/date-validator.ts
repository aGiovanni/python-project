/**
 * @funtion dateValidator.
 *
 * Validates the date based on the input it receives, note that this
 * function only validates it's format based on a regular expression,
 * it can't properly validate the date.
 */

import { ValidatorFn, AbstractControl } from '@angular/forms';

export function dateValidator(regexp: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const isValid = !regexp.test(control.value);
    return isValid ? {'birthday': {value: control.value}} : null;
  };
}
