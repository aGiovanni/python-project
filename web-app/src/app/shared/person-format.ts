/**
 * @interface Person.
 *
 * It encapsulates the person information based on the 'person.txt' format.
 */

export interface Person {
  id?: string
  name?: string
  last_name?: string
  last_mother_name?: string
  job?: string
  company?: string
  street?: string
  exterior_number?: string
  interior_number?: string
  colony?: string
  municipality?: string
  state?: string
  postal_code?: string
  phone_number?: string
  email_address?: string
  birthday?: string
}
