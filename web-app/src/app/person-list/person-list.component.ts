import { Component, OnInit, Input } from '@angular/core';

import { DataService } from '../services/data.service';
import { Person } from '../shared/person-format';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit {

  @Input() persons: Array<Person> = [];

  constructor( private _data: DataService ) { }

  ngOnInit(): void {
  }

  public isPersonsNotEmpty(): boolean { return this.persons.length > 0; }

  public selectPerson( person_id: string ): void {
    this._data.selectPerson( person_id );
  }

  public clearPerson(): void { this._data.clearPerson(); }

  public reloadPersons(): void {
    this._data.fetchPersons();
  }

  public hasPerson(): boolean {
    return this._data.personState ? true : false;
  }

}
