import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-person-email',
  templateUrl: './person-email.component.html',
  styleUrls: ['./person-email.component.scss']
})
export class PersonEmailComponent {

  public option: string = '1';
  public officeType: string = 'txt';

  constructor(
    public dialogRef: MatDialogRef<PersonEmailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onSubmit(): void {
    this.dialogRef.close( { data: this.data.email,
                            officeType: this.officeType,
                            confirm: true } );
  }

  checkOption(): boolean {
    return parseInt(this.option) != 2 ? true : false;
  }

}
