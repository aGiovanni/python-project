import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-ask-confirmation',
  templateUrl: './ask-confirmation.component.html',
  styleUrls: ['./ask-confirmation.component.scss']
})

export class AskConfirmationComponent {

  constructor(
    public dialogRef: MatDialogRef<AskConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  confirmation( response: boolean ): void {
    this.dialogRef.close( response );
  }

}
