# Proyecto 2do parcial - Notas generales:

![Welcome screen](screenshots/screenshot-0.png)
![Add new person](screenshots/screenshot-4.png)
![Edit person](screenshots/screenshot-1.png)
![Delete person](screenshots/screenshot-2.png)
![Send email](screenshots/screenshot-3.png)


Para replicar el proyecto, se necesita tener instalado lo siguiente:

## Aplicación web

  - Node.js, preferentemente la última versión
    LTS. (https://nodejs.org/en/download/)
  - Toolkit de Angular para replicar el ambiente de
    desarrollo. (https://cli.angular.io/)

## API REST Python

  - Pipenv para replicar el ambiente de desarrollo en Python de manera
    aislada al
    sistema. (https://pipenv-fork.readthedocs.io/en/latest/)
  
## Configuración

Primeramente, se alojan todos los archivos en un mismo directorio y
después: `cd nombre_del_directorio`

## Para replicar el API REST:

  - Se ejecuta `$ pipenv install`. Esto instalará las dependencias del
    proyecto en Python en un entorno aislado al sistema operativo.
  - Después, se ejecuta `$ pipenv shell` para el entorno virtual de
    Python asilado del sistema.
  - Se agregan las variables de entorno necesarias mediante los
    comandos: `$ export FLASK_APP=main.py` `$ export
    FLASK_ENV=development` -> Opcional, para habilitar la recarga
    automática de la aplicación cada vez que se cambia el código.
  - Finalmente, se ejecuta `$ flask run` para inicar la API REST. La
    consola mostrará la dirección en donde se estará recibiendo
    peticiones.
  
Se asume que la dirección en donde el servidor estará recibiendo peticiones es `localhost:5000`, esto con el propósito de explicar más o menos las rutas que acepta el servidor:

  - `localhost:5000/persons`: 
    - [GET] -> Regresa la lista de todas las personas registradas en el archivo `persons.txt`.
    - [POST] -> Agrega un usuario nuevo al archivo `persons.txt`
  - `localhost:5000/persons/<id>`: 
    - [GET] -> Regresa el usuario basado en el id dado en la dirección.
    - [POST] -> Actualiza el usuario basado en el id dado en la dirección.
    - [DELETE] -> Eliminar el usuario basado en el id dado en la dirección.
  - `localhost:5000/offices/<id>`:
    - [POST] -> Genera el archivo oficio en base al id del usuario dado en la dirección.
    
Cada attributo que debe de llevar cada ruta está más o menos definido
en el archivo `main.py`, el cual contiene toda la lógica para
interpretar las rutas. El archivo, `interpreter.py` contiene toda la
lógica para administrar el archivo en donde se obtienen los usuarios y
la plantilla de oficio, además de contener también la lógica para
generar archivos PDF.

## Para replicar la aplicación web:

  - Descomprimir el archivo `web-app.zip`.
  - `$ cd web-app/` una vez que el comando anterior haya terminado.
  - Ejecutar `$ npm install`.
  - Ejecutar `$ ng serve` para iniciar el ambiente de desarrollo de la aplicación web, que usualmente se encuentra en: `localhost:4200/`

## Notas para enviar por correo:

  - Se configura las siguientes variables en `main.py` para configurar el correo que estará mandando los oficios:
  ```python
  app.config['MAIL_USERNAME'] = 'correo@gmail.com'
  app.config['MAIL_PASSWORD'] = 'Contraseña'
  app.config['MAIL_DEFAULT_SENDER'] = 'correo@gmail.com'
  ```
  - La cuenta que se configure tiene que tener el acceso disponible para aplicaciones de terceros, cosa que se puede configurar aquí: https://myaccount.google.com/lesssecureapps
