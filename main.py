"""main.py - Main file

In this file, it handles the request made by the Flask module.
"""

from flask import (Flask,
                   request,
                   jsonify)             # Web server imports.
from flask_mail import Mail, Message    # Mail support.
from interpreter import Interpreter     # File handler import.
import json                             # JSON support.
from typing import (List,
                    Dict,
                    Any)                # Type checking with mypy.

# Make the server instance:
app = Flask(__name__)

# Configure the mail server:
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'your_gmail@gmail.com'
app.config['MAIL_PASSWORD'] = 'your_password'
app.config['MAIL_DEFAULT_SENDER'] = 'your_email@gmail.com'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

# Make the mail server instance:
mail = Mail(app)

# Make the interpreter instance
file_handler: Interpreter = Interpreter()


@app.route('/')
def hello_world() -> str:
    return json.dumps({'message': 'Hello world!'})


@app.route('/users', methods=['GET', 'POST'])
def get_users():
    """
    Returns all the available persons and add a new person through a
    POST method.

    The format in which the persons are added should be the same as
    the persons defined in 'persons.txt'.
    """
    response: Any = { 'message': 'Ok' }, 200
    if request.method == 'POST':
        was_successful: bool = file_handler.add_person(request.get_json())
        if not was_successful:
            response = { 'message': 'An error ocurred.' }, 400
    else:
        persons: List[Dict[str, str]] = file_handler.load_persons()
        response = jsonify(persons), 200
    return response


@app.route('/users/<int:user_id>', methods=['GET', 'POST', 'DELETE'])
def operate_user(user_id: int):
    """
    Operate a specific person identified. It supports many methods
    request:
    
    [GET]     -> Obtain the person through it's id.
    [POST]    -> Modify the person through it's id.
                 The client's body format should be the same as 
                 'persons.txt' file to operate correctly
    [DELETE]  -> Delete the person through it's id.
    """
    response: Any = {'message': 'User not found.'}, 400
    valid_response: bool = False
    if request.method == 'POST':
        valid_response = file_handler.modify_person(user_id,
                                                    request.get_json())
        if valid_response:
            response = {'message': 'User modified.'}, 200
    elif request.method == 'DELETE':
        valid_response = file_handler.delete_person(user_id)
        if valid_response:
            response = {'message': 'User deleted.'}, 200
    else:
        person: Dict[str, str] = file_handler.load_person(user_id)
        if not not person:
            response = person, 200
    return response


@app.route('/offices', methods=['POST'])
def batch_generate_office():
    """TODO: Generate offices by batch proccesing, idk."""
    return 'This is a work in progress!'


@app.route('/offices/<int:user_id>', methods=['POST'])
def generate_office(user_id: int):
    """
    Generate invididual office documents, based on the user_id.
    Requires 2 parameters in POST method:
    - send_to_email    -> [boolean] whether to send the email or send locally.
    - office_type      -> [string] It checks 2 strings:
                          - 'plain' - Sends the office in plain text format.
                          - 'pdf'   - Sends the office in pdf format.
    - email_recipient  -> [string] the email address to send the office.
    """
    body: Dict[str, Any] = request.get_json()
    response: Any = {'message': 'Ok'}, 200
    # Parameters is not empty:
    if not body:
        response = {'message': 'Error: Empty body.'}, 400
    else:
        # Get the person.
        person: Dict[str, Any] = file_handler.load_person(user_id)
        if not person:
            response = {'message': 'User not found.'}, 400
        else:
            # Get the office template.
            office_text: Any = file_handler.create_office(person)
            filename: str = f"office-{user_id}"
            if body['office_type'] == 'txt':
                filename += '.txt'
                with open(filename, 'w') as office_file:
                    office_file.write(office_text)
            elif body['office_type'] == 'pdf':
                filename += '.pdf'
                pdf_file = file_handler.generate_pdf(office_text)
                pdf_file.output(filename)
            # Send locally or by email.
            if body['sent_to_email']:
                subject: str = f"Office document for {person['name']}"
                file_type: str = 'text/plain' \
                    if body['office_type'] == 'txt' else 'application/pdf'
                msg = Message(subject,
                              recipients=[ body['email_recipient'] ])
                msg.body = "Sent using Flask-Mail. " + \
                    "see: https://pythonhosted.org/Flask-Mail/"
                with app.open_resource(filename) as fp:
                    msg.attach(filename, file_type, fp.read())
                mail.send(msg)
    return response


@app.after_request
def after_request(response):
    """Quick fix for CORS responses. Since it's executed in localhost,
    there's no need to configure it."""
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Allow-Headers'] = '*'
    header['Access-Control-Allow-Methods'] = '*'
    return response
